function buildCrudRouter(router, repo) {
    router.get('/search', function (req, res) {
        repo.search((err, results) => {
            res.send(results);
        });
    })

    router.get('/', function (req, res) {
        repo.get(req.query.id, (err, result) => {
            res.send(result);
        });
    })

    router.post('/', function (req, res) {
        repo.post(req.body, (err, result) => {
            res.send(result);
        });
    })

    router.put('/', function (req, res) {
        repo.put(req.query.id, req.body, (err, result) => {
            res.send(result);
        });
    })

    router.delete('/', function (req, res) {
        repo.remove(req.query.id,(err, result) => {
            res.send(result);
        });
    })
}
module.exports = {
    buildCrudRouter
};
