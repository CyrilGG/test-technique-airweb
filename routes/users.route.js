const router = require('express').Router();
const usersService = new (require('../services/users.service'))();
const crudRouterBuilder = require('./crud-router-builder');

crudRouterBuilder.buildCrudRouter(router, usersService);

module.exports = router;
