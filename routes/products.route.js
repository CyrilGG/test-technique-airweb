const router = require('express').Router();
const productsService = new (require('../services/products.service'))();
const crudRouterBuilder = require('./crud-router-builder');

crudRouterBuilder.buildCrudRouter(router, productsService);

module.exports = router;
