const router = require('express').Router();
const categoriesService = new (require('../services/categories.service'))();
const crudRouterBuilder = require('./crud-router-builder');

crudRouterBuilder.buildCrudRouter(router, categoriesService);

module.exports = router;
