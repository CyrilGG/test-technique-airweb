const router = require('express').Router();

router.use('/products', require('./products.route'))
router.use('/categories', require('./categories.route'))
router.use('/users', require('./users.route'))

module.exports = router;
