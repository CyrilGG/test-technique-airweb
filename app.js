const express = require('express');
const app = express();
const port = 4000;

require('./sqlite'); // to initiate connexion at start

app.use(require('express').json());
app.use(require('./routes'));

app.listen(port, () => {
    console.log(`App listening on port: ${port}`)
})
