const AbstractRepo = require('./abstract-crud.repo');


class ProductRepo extends AbstractRepo {
    constructor() {
        super(
            'products',
            [
                'label',
                'description',
                'price',
                'category_id',
                'thumbnail_url',
                'visible_public',
                'visible_authenticated'
            ]
        );
    }
}

module.exports = ProductRepo;
