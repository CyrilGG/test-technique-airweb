const AbstractRepo = require('./abstract-crud.repo');


class CategoriesRepo extends AbstractRepo {
    constructor() {
        super(
            'categories',
            [
                'index',
                'label',
                'description'
            ]
        );
    }
}

module.exports = CategoriesRepo;
