const AbstractRepo = require('./abstract-crud.repo');


class UserRepo extends AbstractRepo {
    constructor() {
        super(
            'users',
            [
                'name',
                'email',
                'password_hash'
            ]
        );
    }
}

module.exports = UserRepo;
