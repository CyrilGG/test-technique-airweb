const db = require("../sqlite");

class AbstractCrudRepo {

    constructor(tableName, attributes) {
        this.tableName = tableName;
        this.attributes = attributes;
        this.sqlSearch = `SELECT * FROM ${tableName}`;
        this.sqlGet = `SELECT * FROM ${tableName} WHERE id = $id`;
        this.sqlPost = this._computeSqlPost(tableName, attributes);
        this.sqlPut = this._computeSqlPut(tableName, attributes);
        this.sqlRemove = `DELETE FROM ${tableName} WHERE id = $id`;
    }

    search(cb) {
        db.all(this.sqlSearch, (err, result) => {
            if (err) {
                console.log(err);
            }
            cb(err, result);
        });
    }

    get(id, cb) {
        db.get(this.sqlGet, {$id: id}, (err, result) => {
            if (err) {
                console.log(err);
            }
            cb(err, result);
        });
    }

    post(product, cb) {
        const fields = this._mapFields(product, this.attributes);
        db.run(this.sqlPost, fields, (err, result) => {
            if (err) {
                console.log(err);
            }
            cb(err, result);
        });
    }

    put(id, product, cb) {
        const fields = this._mapFields(product, this.attributes);
        db.run(this.sqlPut, {...fields, $id: id}, (err, result) => {
            if (err) {
                console.log(err);
            }
            cb(err, result);
        });
    }

    remove(id, cb) {
        db.run(this.sqlRemove, {$id: id}, (err, result) => {
            if (err) {
                console.log(err);
            }
            cb(err, result);
        });
    }

    _mapFields(obj, attributes) {
        return attributes.reduce((acc, cur) => {
            return {...acc, ['$' + cur]: obj[cur]}
        }, {})
    }

    _computeSqlPost(tableName, attributes) {
        return `INSERT INTO ${tableName} (${attributes.map(this._escapeReservedWords).join(',')}) VALUES (${attributes.map(e => `$${e}`).join(',')})`
    }

    _computeSqlPut(tableName, attributes) {
        return `UPDATE ${tableName} SET ${attributes.map(this._escapeReservedWords).map(e => `${e} = $${e}`).join(',')} WHERE id = $id`
    }

    _escapeReservedWords(str) {
        if (['index'].includes(str)) {
            return `[${str}]`;
        }
        return str;
    }

}

module.exports = AbstractCrudRepo;
