const sqlite3 = require("sqlite3").verbose();
const dbFilename = 'DATABASE.sqlite';

const db = new sqlite3.Database(dbFilename, err => {
    if (err) {
        return console.error(err.message);
    }
    console.log("Database connected");
});

module.exports = db;
