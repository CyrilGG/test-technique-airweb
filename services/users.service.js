const AbstractCrudService = require('./abstract-crud.service');
const usersRepo = new (require('../repositories/users.repo'))();
const crypto = require('crypto');

class UsersService extends AbstractCrudService {
    constructor() {
        super(usersRepo);
    }

    post(obj, cb) {
        obj.password_hash = this._getPasswordHash(obj.email, obj.password);
        return this.repo.post(obj, cb);
    }

    put(id, obj, cb) {
        obj.password_hash = this._getPasswordHash(obj.email, obj.password);
        return this.repo.put(id, obj, cb);
    }

    _getPasswordHash(email, password) {
        return this._hashMD5(email + password);
    }

    _hashMD5(str) {
        return crypto.createHash('md5').update(str).digest("hex");
    }
}

module.exports = UsersService;
