const AbstractCrudService = require('./abstract-crud.service');
const categoriesRepo = new (require('../repositories/categories.repo'))();

class CategoriesService extends AbstractCrudService {
    constructor() {
        super(categoriesRepo);
    }
}

module.exports = CategoriesService;
