class AbstractCrudService {

    constructor(repo) {
        this.repo = repo;
    }

    search(cb) {
        return this.repo.search(cb);
    }

    get(id, cb) {
        return this.repo.get(id, cb);
    }

    post(obj, cb) {
        return this.repo.post(obj, cb);
    }

    put(id, obj, cb) {
        return this.repo.put(id, obj, cb);
    }

    remove(id, cb) {
        return this.repo.remove(id, cb);
    }

}

module.exports = AbstractCrudService;
