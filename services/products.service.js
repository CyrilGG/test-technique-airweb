const AbstractCrudService = require('./abstract-crud.service');
const productsRepo = new (require('../repositories/products.repo'))();

class ProductsService extends AbstractCrudService {
    constructor() {
        super(productsRepo);
    }
}

module.exports = ProductsService;
